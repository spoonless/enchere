package io.spoonless.enchere.controller;

import java.math.BigDecimal;
import java.net.URI;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.spoonless.enchere.service.Enchere;

public class EnchereAdapter {
	
	private Enchere enchere;

	public EnchereAdapter(Enchere enchere) {
		this.enchere = enchere;
	}

	public String getProduit() {
		return enchere.getProduit().getNom();
	}

	@JsonProperty("url_image")
	public String getUrlImage() {
		UriComponentsBuilder uriBuilder = MvcUriComponentsBuilder.fromController(EnchereController.class);
		return uriBuilder.replacePath(enchere.getProduit().getUrlImage().getPath()).toUriString();
	}

	@JsonProperty("url_enchere")
	public URI getUrlEnchere() {
		UriComponentsBuilder uriBuilder = MvcUriComponentsBuilder.fromController(EnchereController.class);
		return uriBuilder.pathSegment("{id}").build(enchere.getId());
	}

	@JsonProperty("prix_base")
	public BigDecimal getPrixBase() {
		return enchere.getPrixBase();
	}

	@JsonProperty("temps_restant")
	public long getTempsRestant() {
		return enchere.getTempsRestant();
	}

	@JsonProperty("prix_actuel")
	public BigDecimal getPrixActuel() {
		return enchere.getPrixActuel();
	}

	public String getAcheteur() {
		return enchere.getAcheteur();
	}

}