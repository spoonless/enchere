package io.spoonless.enchere.controller;

import java.math.BigDecimal;

public class NouvelleEnchereDto {

	private String pseudo;
	private BigDecimal prix;

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public BigDecimal getPrix() {
		return prix;
	}

	public void setPrix(BigDecimal prix) {
		this.prix = prix;
	}
	
	public void valider() throws ValidationException {
		if (pseudo == null || pseudo.isBlank()) {
			throw new ValidationException("Il manque le pseudo.");
		}
		if (prix.compareTo(BigDecimal.ZERO) <= 0) {
			throw new ValidationException("Le prix doit être strictement positif.");
		}
	}

}
