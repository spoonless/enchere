package io.spoonless.enchere.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.spoonless.enchere.service.EnchereNonTrouveeException;
import io.spoonless.enchere.service.EnchereService;
import io.spoonless.enchere.service.EnchereTermineeException;

@RestController
@RequestMapping(path = "/enchere")
public class EnchereController {

	@Autowired
	private EnchereService enchereService;

	@ExceptionHandler(EnchereNonTrouveeException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handle(EnchereNonTrouveeException e) {
		return "L'enchère n'existe pas.";
	}

	@ExceptionHandler(EnchereTermineeException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String handle(EnchereTermineeException e) {
		return "L'enchère est terminée.";
	}
	
	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String handle(ValidationException e) {
		return e.getMessage();
	}

	@GetMapping(produces = "application/json")
	public EnchereAdapter getEnCours() {
		return new EnchereAdapter(enchereService.getEnchereEnCours());
	}

	@GetMapping(path = "/{enchereId}", produces = "application/json")
	public EnchereAdapter get(@PathVariable String enchereId) throws EnchereNonTrouveeException {
		return new EnchereAdapter(enchereService.getEnchere(enchereId));
	}

	@PostMapping(path = "/{enchereId}", produces = "application/json", consumes = "application/json")
	public EnchereAdapter encherir(@PathVariable String enchereId, @RequestBody NouvelleEnchereDto nouvelleEnchere) throws EnchereNonTrouveeException, EnchereTermineeException, ValidationException {
		nouvelleEnchere.valider();
		return new EnchereAdapter(enchereService.encherir(enchereId, nouvelleEnchere.getPseudo(), nouvelleEnchere.getPrix()));
	}
}
