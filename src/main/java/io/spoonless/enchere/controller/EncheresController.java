package io.spoonless.enchere.controller;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.spoonless.enchere.service.Enchere;
import io.spoonless.enchere.service.EnchereService;

@RestController
@RequestMapping(path = "/encheres")
public class EncheresController {

	@Autowired
	private EnchereService enchereService;

	@GetMapping(produces = "application/json")
	public Stream<EnchereAdapter> getAll(@RequestParam(required = false)  String pseudo) {
		if (pseudo == null || pseudo.isEmpty()) {
			return enchereService.getAll().stream().filter(Enchere::isTerminee).map(EnchereAdapter::new);
		}
		return enchereService.getAll().stream().filter(Enchere::isTerminee).filter(e -> pseudo.equals(e.getAcheteur())).map(EnchereAdapter::new);
	}
}
