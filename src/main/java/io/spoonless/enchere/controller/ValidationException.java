package io.spoonless.enchere.controller;

public class ValidationException extends Exception {

	private static final long serialVersionUID = -8240797800938814646L;

	public ValidationException(String message) {
		super(message);
	}

}
