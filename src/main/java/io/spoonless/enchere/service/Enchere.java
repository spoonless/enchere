package io.spoonless.enchere.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class Enchere implements Cloneable {
	
	private String id;
	private Produit produit;
	private BigDecimal prixActuel;
	private BigDecimal prixGagnant;
	private String acheteur = "";
	private long tempsFin;
	
	public  Enchere(Produit produit, int dureeEnchereSecondes) {
		this.id = UUID.randomUUID().toString();
		this.produit = produit;
		this.prixActuel = produit.getPrix();
		this.prixGagnant = produit.getPrix();
		this.tempsFin = System.currentTimeMillis() + (dureeEnchereSecondes * 1000) + 1000;
	}
	
	public String getId() {
		return id;
	}
	
	public boolean isTerminee() {
		return getTempsRestant() == 0;
	}

	public long getTempsRestant() {
		long now = System.currentTimeMillis();
		return Math.max(0, (tempsFin - now) / 1000);
	}
	
	public BigDecimal getPrixBase() {
		return produit.getPrix();
	}

	public BigDecimal getPrixActuel() {
		if (isTerminee()) {
			if (! this.prixActuel.equals(this.produit.getPrix()) && ! this.prixActuel.equals(this.prixGagnant)) {
				return this.prixActuel.add(BigDecimal.ONE);
			}
		}
		return this.prixActuel;
	}
	
	public void encherir(String acheteur, BigDecimal prix) {
		if (isTerminee()) {
			return;
		}
		prix = prix.setScale(2, RoundingMode.DOWN);
		if (prix.compareTo(prixGagnant) > 0) {
			this.prixActuel = this.prixGagnant;
			this.prixGagnant = prix;
			this.acheteur = acheteur;
		} else if (prix.compareTo(prixActuel) > 0) {
			this.prixActuel = prix;
		}
	}

	public Produit getProduit() {
		return produit;
	}

	public String getAcheteur() {
		return acheteur;
	}
	
	@Override
	public Enchere clone() {
		try {
			return (Enchere) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException("Clonage échoué (ce n'est pas possible normalement)");
		}
	}
}
