package io.spoonless.enchere.service;

import static java.util.Optional.ofNullable;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EnchereService {
	
	@Autowired
	private ProduitService produitService;
	@Value("${enchere.duree.secondes}")
	private int dureeEnchereEnSecondes;
	@Value("${enchere.max}")
	private int maxEncheres;
	private Enchere enchereEnCours;
	private Map<String, Enchere> encheres = new LinkedHashMap<>() {
		private static final long serialVersionUID = 1L;

		protected boolean removeEldestEntry(Map.Entry<String,Enchere> eldest) {
			return this.size() > maxEncheres;
		};
	};
	
	public synchronized Enchere getEnchereEnCours() {
		if (enchereEnCours.isTerminee()) {
			creerEnchere();
		}
		return enchereEnCours.clone();
	}

	public synchronized Enchere getEnchere(String id) throws EnchereNonTrouveeException {
		return getEnchereOriginale(id).clone();
	}

	public synchronized Enchere encherir(String id, String encherisseur, BigDecimal prix) throws EnchereNonTrouveeException, EnchereTermineeException {
		Enchere enchere = this.getEnchereOriginale(id);
		if (enchere.isTerminee()) {
			throw new EnchereTermineeException();
		}
		enchere.encherir(encherisseur, prix);
		return enchere.clone();
	}
	
	public synchronized Collection<Enchere> getAll() {
		return this.encheres.values();
	}

	private Enchere getEnchereOriginale(String id) throws EnchereNonTrouveeException {
		return ofNullable(encheres.get(id)).orElseThrow(EnchereNonTrouveeException::new);
	}
	
	@PostConstruct
	private void creerEnchere() {
		enchereEnCours = new Enchere(produitService.getProduit(), dureeEnchereEnSecondes);
		encheres.put(enchereEnCours.getId(), enchereEnCours);
	}
}
