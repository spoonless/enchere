package io.spoonless.enchere.service;

import java.io.IOException;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ProduitService {
	
	private static final String PRODUITS_JSON_PATH = "/produits.json";
	private Produit[] produits;
	
	@PostConstruct
	private void chargerProduits() throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		produits = objectMapper.readValue(getClass().getResourceAsStream(PRODUITS_JSON_PATH), Produit[].class);
	}
	
	public Produit getProduit() {
		int index = new Random().nextInt(produits.length);
		return produits[index];
	}

}
