package io.spoonless.enchere.service;

import java.math.BigDecimal;
import java.net.URI;

public class Produit {
	
	private String nom;
	private URI urlImage;
	private BigDecimal prix;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public URI getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(URI urlImage) {
		this.urlImage = urlImage;
	}

	public BigDecimal getPrix() {
		return prix;
	}

	public void setPrix(BigDecimal prix) {
		this.prix = prix;
	}

}
